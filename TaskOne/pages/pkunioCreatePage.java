package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class pkunioCreatePage {

	WebDriver driver;

	By findBoxCreate = By.id("quick-create-page-button");
	By findTitle = By.id("content-title");
	By findBoxSave = By.id("rte-button-publish");
	By checkTitleText =By.id("title-text");
	

	public pkunioCreatePage(WebDriver driver){
		this.driver = driver;
	}



	//click to create new page
	
	public void clickCreatePage(){
		
		driver.findElement(findBoxCreate).click();

	}

	//add title

	public void setitle(String newTitle){

		driver.findElement(findTitle).sendKeys(newTitle);;

	}

	//save page
	
	public void savePage(){
		driver.findElement(findBoxSave).click();

	}

	public void addTitle(String newTitle){

		this.setitle(newTitle);

	}

	public String getPageTitle(){

		return    driver.findElement(checkTitleText).getText();

	}


	public void createPage(String newTitle){

		this.clickCreatePage();

		this.setitle(newTitle);

		this.savePage();
	}


}
