package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



public class pkunioLogin {

	WebDriver driver;

	By pkunioName = By.name("username");
	By passwordpkunio = By.name("password");
	By titleText =By.name("Atlassian Cloud");
	By login = By.id("login");



	public pkunioLogin(WebDriver driver){

		this.driver = driver;

	}

	//Set user name in textbox

	public void setUserName(String strUserName){

		driver.findElement(pkunioName).sendKeys(strUserName);;

	}



	//Set password in password textbox

	public void setPassword(String strPassword){

		driver.findElement(passwordpkunio).sendKeys(strPassword);

	}



	//Click on login button

	public void clickLogin(){

		driver.findElement(login).click();

	}



	//Get the title of Login Page

	public String getLoginTitle(){

		return    driver.findElement(titleText).getText();

	}



	//This POM method will be exposed in test case


	public void loginTopkunio(String strUserName,String strPasword){

		//Fill user name

		this.setUserName(strUserName);

		//Fill password

		this.setPassword(strPasword);

		//Click Login button

		this.clickLogin();        



	}

}
