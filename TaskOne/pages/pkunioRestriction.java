package pages;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class pkunioRestriction {
	
	WebDriver driver;
	
	By findDropMenu = By.id("action-menu-link");
	By findRestrictionButton = By.id("action-page-permissions-link");
	By findDropMenuRes = By.id("s2id_page-restrictions-dialog-selector");
	By findOption = By.xpath("//ul[@class='select2-results']/li[2]") ;
	By findAddButton = By.id("page-restrictions-add-button");
	By applyButton = By.id("page-restrictions-dialog-save-button");
	
	
	public pkunioRestriction(WebDriver driver){
		this.driver = driver;
	}
	
	//find drop down menu
	
	public void clickDropDown(){
		
		driver.findElement(findDropMenu).click();
		
		

	}
	
	public void selectRestriction(){
		
		driver.findElement(findRestrictionButton).click();
		
	}
	
	public void selectDropDownRes(){
		
		driver.findElement(findDropMenuRes).click();
			

	}
	
	public void getCorrectOption(){
		
		driver.findElement(findOption).click();
			
				
		}
	
	public String getAddButtonRes() {
		
		return driver.findElement(findAddButton).getText();
	}
	

	public void applyRestrictions(){
		
		driver.findElement(applyButton).click();
			
				
		}
	
		
	
	public void addRestriction(){

		this.clickDropDown();
		this.selectRestriction();
		this.selectDropDownRes();
		this.getCorrectOption();
		
		//check add restriction button
		String addButtonRestr = getAddButtonRes();
		Assert.assertTrue(addButtonRestr.contains("Add"));
		
		this.applyRestrictions();
	
		
	}

}
