package tst;

import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pages.pkunioLogin;
import pages.pkunioCreatePage;
import pages.pkunioRestriction;



public class AtlassianTst1POM {
	WebDriver driver;

	pkunioLogin objLogin;
	pkunioCreatePage objCreatePage;
	pkunioRestriction objRestriction;


	@Before
	public void setUp() {

		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://pkuniniec.atlassian.net/login?dest-url=/wiki%2Findex.action&permission-violation=true");

	}

	@Test

	public void test_Login_Page_Appear_Correct(){

		//login to page
		String randomNewTitle =  RandomStringUtils.random(5,"abcdefghijklmnoprstuwxyz");

		objLogin = new pkunioLogin(driver);

		objLogin.loginTopkunio("p.kunio@icloud.com", "geniusz8888");


		//create new page
		objCreatePage = new pkunioCreatePage(driver);

		objCreatePage.createPage(randomNewTitle);

		//check created page
		String PageTitle = objCreatePage.getPageTitle();

		Assert.assertTrue(PageTitle.contains(randomNewTitle));


		//is restriction available
		objRestriction = new pkunioRestriction(driver);

		objRestriction.addRestriction();





	}

}




